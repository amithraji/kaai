<?php require_once('engine/lib/template_modules.php'); 
 require_once('engine/lib/functions.php'); session_start(); ?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="KAAI">

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/selectize.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/css/vanillabox/vanillabox.css" type="text/css">

    <link rel="stylesheet" href="assets/css/style.css" type="text/css">

    <title>Jobs - KAAI</title>

</head>

<body class="page-sub-page page-course-listing">
<!-- Wrapper -->
<div class="wrapper">
<!-- Header -->
<?php include_once("engine/parts/header.php");  print_header("career");?>
<!-- end Header -->

<!-- Breadcrumb -->
<div class="container">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li>Career</li>
        <li class="active">Jobs</li>
    </ol>
</div>
<!-- end Breadcrumb -->

<!-- Page Content -->
<div id="page-content">
    <div class="container">
        <div class="row">
            <!--MAIN Content-->
            <div class="col-md-12">
                <div id="page-main">
                    <section class="course-listing" id="courses">
                        <header><h1>Jobs</h1></header>
                        
                        <section id="course-search">
                            <div class="search-box">
                                <header><span class="fa fa-search"></span><h2>Find Jobs for You</h2></header>
                                <form id="course-search-form" role="form" class="form-inline">
                                    <div class="form-row">
                                        <div class="form-group">
                                            <label for="course-type">Job Type</label>
                                            <select name="course-type" id="course-type">
                                                <option value="">Graphic Design and 3D</option>
                                                <option value="2">History and Psychology</option>
                                                <option value="3">Marketing</option>
                                                <option value="4">Science</option>
                                            </select>
                                        </div><!-- /.form-group -->

                                        <div class="form-group">
                                            <label for="study-level">Experience Level</label>
                                            <select name="study-level" id="study-level">
                                                <option value="">Study Level</option>
                                                <option value="2">Beginner</option>
                                                <option value="3">Advanced</option>
                                                <option value="4">Intermediate</option>
                                                <option value="5">Professional</option>
                                            </select>
                                        </div><!-- /.form-group -->

                                        <div class="form-group">
                                            <label for="full-text">Salary</label>
                                            <input name="full-text" id="full-text" placeholder="Enter Keyword" type="text">
                                        </div><!-- /.form-group -->
                                    </div>
                                    <button type="submit" class="btn pull-right">Search</button>
                                </form><!-- /#<!-- /.form-group -->
                            </div><!-- /.course-search-box -->
                        </section><!-- /#course-search -->

                        <div class="row">
                            
                            <div class="col-md-4 col-sm-4">
                                <article class="course-thumbnail" style="text-align: center;margin: 5px;">
                                    <div class="description">
                                        <a href="course-detail-v1.html"><h3>Web Developer</h3></a>
                                        <a href="#" class="c    ourse-category">Software</a>
                                        <hr>
                                        <div class="course-meta">
                                            <span class="course-date"><i class="fa fa-heartbeat"></i>0-5 years</span>
                                            <span class="course-length"><i class="fa fa-money"></i>400k/year</span>
                                        </div>
                                        <hr>
                                        <span>Bash Labs Inc.</span>
                                    </div><br>
                                        <a href="#" class="stick-to-bottom btn btn-framed btn-color-grey btn-small" style="width: 90%;position:relative; left:auto;right:auto;">View Details</a>
                                </article><!-- /.featured-course -->
                            </div><!-- /.col-md-3 -->
                            <div class="col-md-4 col-sm-4">
                                <article class="course-thumbnail" style="text-align: center;margin: 5px;">
                                    <div class="description">
                                        <a href="course-detail-v1.html"><h3>Web Developer</h3></a>
                                        <a href="#" class="c    ourse-category">Software</a>
                                        <hr>
                                        <div class="course-meta">
                                            <span class="course-date"><i class="fa fa-heartbeat"></i>0-5 years</span>
                                            <span class="course-length"><i class="fa fa-money"></i>400k/year</span>
                                        </div>
                                        <hr>
                                        <span>Bash Labs Inc.</span>
                                    </div><br>
                                        <a href="#" class="stick-to-bottom btn btn-framed btn-color-grey btn-small" style="width: 90%;position:relative; left:auto;right:auto;">View Details</a>
                                </article><!-- /.featured-course -->
                            </div><!-- /.col-md-3 -->
                        </div><!-- /.row -->
                    </section><!-- /.course-listing -->
                    <div class="center">
                        <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                        </ul>
                    </div>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->

        
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
<!-- end Page Content -->

<!-- Footer -->
<?php include_once("engine/parts/footer.php");  ?>

<!-- end Footer -->

</div>
<!-- end Wrapper -->

<script type="text/javascript" src="assets/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/selectize.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="assets/js/jQuery.equalHeights.js"></script>
<script type="text/javascript" src="assets/js/icheck.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.vanillabox-0.1.5.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="assets/js/retina-1.1.0.min.js"></script>

<script type="text/javascript" src="assets/js/custom.js"></script>

</body>
</html>