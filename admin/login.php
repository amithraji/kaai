<?php session_start(); if(!empty($_SESSION['username'])){header('location: ./index.php');} ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login -> Alumni Database</title>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/admin.css" rel="stylesheet" type="text/css" />
</head>
<body class="light_theme  fixed_header left_nav_fixed">
<div class="wrapper">
    
  <!--\\\\\\\ wrapper Start \\\\\\-->
  <div class="login_page">
  <div class="login_content">
  <div class="panel-heading border login_heading">sign in now</div>	
 <form role="form" class="form-horizontal">
      <div class="form-group">
        
        <div class="col-sm-10">
          <input type="text" placeholder="Email" id="username" class="form-control" name="u">
        </div>
      </div>
      <div class="form-group">
        
        <div class="col-sm-10">
          <input type="password" placeholder="password" id="password" class="form-control" name="p">
        </div>
          
      </div>
      <div class="form-group">
        <div class=" col-sm-10">
          <div class="checkbox checkbox_margin">
              <button class="btn btn-default" type="button" id="login" onclick="logIn()">Login</button>
            </div>
        </div><br>
      </div>
      
    </form>
 </div>
  </div>
  
  
  
  
</div>
<!--\\\\\\\ wrapper end\\\\\\-->
<!-- Modal -->

<!-- /sidebar chats -->   





<script src="js/jquery-2.1.0.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/common-script.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
    <script>
        function logIn(){
    var user = $('#username').val();
	var password= $('#password').val();
    
var logString= 'username='+user+'&password='+password+'&login=true';
 
    $.ajax({
            type: "POST",
            url: "php/actions.php",
            data: logString,
            cache: false,
            beforeSend: function(){ $("#login").html('Logging In <i class="fa fa-spin fa-spinner"></i>');},
            success: function(data){
            if(data)
            {
                if(data=="done"){
//                    alert(data);
		       $('#suc_mess').show();
                $("#login").html('Successfull');
                    location.reload();
            }   
            else{
                $("#login").html('Try Again');
                		       $('#err_mess').show();

//            alert(data);
                
            }
            }
            else
            {
             alert('no response from server');
            }
            }
            });
}
        </script>
</body>
</html>
