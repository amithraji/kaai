-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 04, 2015 at 03:52 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alumni_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumnus`
--

CREATE TABLE IF NOT EXISTS `alumnus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(33) NOT NULL,
  `email` varchar(333) NOT NULL,
  `phone` varchar(333) NOT NULL,
  `regno` varchar(33) NOT NULL,
  `batch` varchar(33) NOT NULL,
  `branch` varchar(33) NOT NULL,
  `company` varchar(33) NOT NULL,
  `permanent_address` longtext NOT NULL,
  `acheivements` text NOT NULL,
  `job` varchar(33) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `alumnus`
--

INSERT INTO `alumnus` (`id`, `name`, `email`, `phone`, `regno`, `batch`, `branch`, `company`, `permanent_address`, `acheivements`, `job`) VALUES
(22, 'B. Devaraj', 'bdevaraj_70@yahoo.co.in', ' 91 932244397', '', '1989', 'CIVIL', ' HDFC Property Ventures Ltd.', 'Plot No:84885, Anusha cls. Flat No A-201,or sector-8, Kopar khairane,Navi Mumbai, Pin-400709, \n', '', 'Vice President'),
(23, 'M Alfred Jayakumar', 'alfred@fluidengg.in', ' 91 9867683516', '', '1989', 'MECHANICAL', 'Business', 'A602 Dheeraj Residency, New Link Road, Goregaon,Mumbai-400104\n', '', ''),
(24, 'Y. Azariah', 'azar71@yahoo.com', ' 91 9886026789', '', '1989', 'Mechanical', '', '31,1st floor,5th cross,Narayanapa block,Benson Town Post, Bengaluru 46\n', '', ''),
(25, 'Binitha Philip', 'BINITHACYRIL@yahoo.com', ' 001-616-206-0327', '', '1989', 'ECE', '', '4212, Oak forest CT,Grand Rapids\n', '', 'IT-Software Engineer'),
(26, 'PU.Vijeindiran', 'vijein1972@yahoo.w.uk', ' 91 9841728750', '', '1989', 'Mechanical', '', '22/12, Kamatchi Amman Street,Ekkattuthangal, Chenai-32\n', '', 'Proprietor'),
(27, 'Mathew George', 'mail4mathew@gmail.com', ' 91 9900510601', '', '1989', 'Mechanical', '', 'A-101,Habitat Aster Apartment,Cauvery Nagar main rd,Hoodi,Bengaluru-560048\n', '', 'Solution Manager'),
(29, 'Dr.P.Senthil Kumar', 'psk@civ.psgtech.ac.in', ' 91 9843444158', '', '1989', 'Civil', 'PSG Tech', '9G,Second street,Balasubramanian Nagar,Anna Nagar,Peelamedu,Coimbatore-641004\n', '', 'Associate Professor'),
(30, 'Anita Joan', 'anitajoan1996@yahoo.co.in', ' 91 9486957719(India),00974669727', '', '1989', 'ECE', '', '', '', 'Home maker'),
(31, 'S.P.Krishnan', 'sp_krishnan@hotmail.com,shreeandshreeent@gmail.com', ' 91 9003924495', '', '1989', 'ECE', 'Inst ', '13D,KPS Palaniappa Complex,Trichy-Chennai Bypass Rd,Trichy-10\n', '', ''),
(32, 'Sunil David', 'sunphi@hotmail.com', ' 91 9840023571', '', '1989', 'ECE', 'ATRT Global Network Services,Indi', '13D,KPS Palaniappa Complex,Trichy-Chennai Bypass Rd,Trichy-11\n', '', 'Regional Manager'),
(33, 'Sunil David', 'sunphi@hotmail.com', ' 91 9840023571', '', '1989', 'ECE', 'ATRT Global Network Services,Indi', '13D,KPS Palaniappa Complex,Trichy-Chennai Bypass Rd,Trichy-11\n', '', 'Regional Manager'),
(34, 'Louis Prem Kumar', 'louisprem@gmail.com', ' 91 9488464454, 2348056491073(Nigeria)', '', '1989', 'Mechanical', '', '13D,KPS Palaniappa Complex,Trichy-Chennai Bypass Rd,Trichy-12\n', '', 'General Manager');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(12) NOT NULL,
  `password` varchar(33) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(3, 'ami', '9447fefa6cde5b06689fa5ec85f1a49c'),
(4, 'jebin', '96170c92ca77f417f9c936d58cac5cc5'),
(6, 'ur13ec152', '8e0c491781615875e7369f5941801bd0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
