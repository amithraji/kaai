
$(document).ready(function() {

    $('#dynamic-table').dataTable( {
        "aaSorting": [[ 4, "desc" ]]
    } );

    /*
     * Insert a 'details' column to the table
     */
    /*var nCloneTh = document.createElement( 'th' );
    var nCloneTd = document.createElement( 'td' );
    nCloneTd.innerHTML = '<img src="plugins/advanced-datatable/images/details_open.png" id="img">';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each( function () {
        this.insertBefore( nCloneTh, this.childNodes[0] );
    } );

    $('#hidden-table-info tbody tr').each( function () {
        this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
    } );*/

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[1, 'asc']]
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    $('.expand-data').click(function () {
        var nTr = $(this).parents('tr')[0];
        if ( oTable.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            this.src = "plugins/advanced-datatable/images/details_open.png";
            oTable.fnClose( nTr );
        }
        else
        {
            var alu_id = this.id;
            /* Open this row */
            this.src = "plugins/advanced-datatable/images/details_close.png";
            oTable.fnOpen( nTr, fetchTableData(alu_id), 'details' );
        }
    } );


  function fetchTableData(id)
{
    var aid = id;
    var classi = '.'+aid;
 $.ajax({
            type: "POST",
            url: "php/actions.php",
            data: 'expand=true&aid='+ aid,
            cache: false,
//            beforeSend: function(){ $(classi).html('<i class="fa fa-spin fa-spinner"></i>');  },
            success: function(s0ut){
            if(s0ut)
            {   
               $('#hidden-data').val(s0ut);
//                $(classi).html();
            }
            else
            {
             alert('no response from server');
            }
//            return true;
            }
            });
    var result = $('#hidden-data').val();
    return result;
}
} );