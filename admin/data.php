<?php session_start(); if(empty($_SESSION['username'])){header('location: ./login.php');} ?>

<?php if(!isset($_GET['action']) || $_GET['action'] != "add" && $_GET['action'] != "edit"){header("location: data.php?action=add");} include_once("php/lib.php");?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Aluni Data</title>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="plugins/toggle-switch/toggles.css" rel="stylesheet" type="text/css" />
<link href="plugins/checkbox/icheck.css" rel="stylesheet" type="text/css" />
<link href="plugins/checkbox/minimal/blue.css" rel="stylesheet" type="text/css" />
<link href="plugins/checkbox/minimal/green.css" rel="stylesheet" type="text/css" />
<link href="plugins/checkbox/minimal/grey.css" rel="stylesheet" type="text/css" />
<link href="plugins/checkbox/minimal/orange.css" rel="stylesheet" type="text/css" />
<link href="plugins/checkbox/minimal/pink.css" rel="stylesheet" type="text/css" />
<link href="plugins/checkbox/minimal/purple.css" rel="stylesheet" type="text/css" />
<link href="plugins/bootstrap-fileupload/bootstrap-fileupload.min.css" rel="stylesheet">
<link href="plugins/dropzone/dropzone.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="plugins/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="plugins/bootstrap-colorpicker/css/colorpicker.css" />
</head>
<body class="light_theme  fixed_header left_nav_fixed">
<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->
  <div class="header_bar">
    <!--\\\\\\\ header Start \\\\\\-->
    <div class="brand">
      <!--\\\\\\\ brand Start \\\\\\-->
      <div class="logo" style="display:block"><span class="theme_color">ALUMNI</span> ADMIN</div>
      <div class="small_logo" style="display:none"><img src="images/s-logo.png" width="50" height="47" alt="s-logo" /> <img src="images/r-logo.png" width="122" height="20" alt="r-logo" /></div>
    </div>
    <!--\\\\\\\ brand end \\\\\\-->
    <div class="header_top_bar">
      <!--\\\\\\\ header top bar start \\\\\\-->
      <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
      <div class="top_left">
        <div class="top_left_menu">
          <ul>
            <li> <a href="javascript:void(0);"><i class="fa fa-repeat"></i></a></li>
          </ul>
        </div>
      </div>
      <a href="javascript:void(0);" class="add_user" data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus-square"></i> <span> New Entry</span> </a>
      <div class="top_right_bar">
        <div class="top_right">
          <div class="top_right_menu">
          </div>
        </div>
        <div class="user_admin dropdown"> <a href="javascript:void(0);" data-toggle="dropdown"><img src="images/user.png" /><span class="user_adminname"><?php echo $_SESSION['username']; ?></span> <b class="caret"></b> </a>
          <ul class="dropdown-menu">
            <div class="top_pointer"></div>
            <li> <a href="profile.html"><i class="fa fa-user"></i> Profile</a> </li>
            <li> <a href="php/actions.php?logout=true"><i class="fa fa-power-off"></i> Logout</a> </li>
          </ul>
        </div>

        
      </div>
    </div>
    <!--\\\\\\\ header top bar end \\\\\\-->
  </div>
  <!--\\\\\\\ header end \\\\\\-->
  <div class="inner">
    <!--\\\\\\\ inner start \\\\\\-->
    <div class="left_nav">
      <div class="left_nav_slidebar">
        <ul>
          <li class="theme_border"><a href="./"><i class="fa fa-home"></i> Dashboard</a>
          </li>
          <li class="left_nav_active theme_border"><a href="data.php"><i class="fa fa-plus"></i> Add Alumni Data </a>
          </li>
          <li class="theme_border"><a href="manage.php"><i class="fa fa-edit"></i> Search/Manage Data  </a>
          </li>
            <?php if($_SESSION['username']=="ami"){ ?>
            <li class="theme_border"><a href="newUser.php"><i class="fa fa-user"></i> Add User  </a>
            </li><?php }?>
        </ul>
      </div>
    </div>
    <!--\\\\\\\left_nav end \\\\\\-->
    <div class="contentpanel">
      <!--\\\\\\\ contentpanel start\\\\\\-->
      <div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1>Data</h1>
          <h2 class="">Add Delete Data</h2>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Add Entry</li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->
        
       <!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Add New Entry</h3>
            </div>
            <div class="porlets-content">
                
                <?php if($_GET['action']=="add") { ?>
              <form class="form-horizontal group-border-dashed" action="#" parsley-validate novalidate>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Full Name</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required placeholder="Type something" id="fullname"/>
                  </div>
                </div>
                  <div class="form-group">
                  <label class="col-sm-3 control-label">Batch</label>
                  <div class="col-sm-6">
                      <select class="form-control" id="batch">
                          <option>Select...</option>
                          <?php for($i=1986;$i<=2015;$i++){
                            $to = $i+4;
                          echo "<option value='$i - $to'>$i - $to</option>";
                           } ?>
                      </select>
                      </div>
                </div><!--/form-group--> 
                  <div class="form-group">
                  <label class="col-sm-3 control-label">Reg No</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required placeholder="UR13CS015" id="regno"/>
                  </div>
                </div>
                  <div class="form-group">
                  <label class="col-sm-3 control-label">Branch</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required placeholder="CSE" id="branch"/>
                  </div>
                </div>
                  <div class="form-group">
                  <label class="col-sm-3 control-label">Permanent Address</label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" id="permanent_address"></textarea></div>
                </div>
                  
                  <div class="form-group">
                  <label class="col-sm-3 control-label">Email</label>
                  <div class="col-sm-6">
                    <input type="email" class="form-control" required placeholder="someone@domain.com" id="email"/>
                  </div>
                </div> 
                  <div class="form-group">
                  <label class="col-sm-3 control-label">Phone Number</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" value="+91 " placeholder="Enter sumthing" required  id="phoneno"/>
                  </div>
                </div>
                  <div class="form-group">
                  <label class="col-sm-3 control-label">Job</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required placeholder="Manager,CEO etc" id="job"/>
                  </div>
                </div>
                  <div class="form-group">
                  <label class="col-sm-3 control-label">Company</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required placeholder="Bash Labs" id="company"/>
                  </div>
                </div>
                  
                  <div class="form-group">
                  <label class="col-sm-3 control-label">Achievements</label>
                  <div class="col-sm-6">
                <textarea class="form-control" id="achievements"></textarea></div>
                </div>
                
                
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" class="btn btn-primary" onclick="addAlumni()" id="add">Submit</button>
                    <button class="btn btn-default">Cancel</button>
                      <span style="color: green; font 9px;display: none;" id="suc_mess">Succcessfully entered.</span>
                  </div>
                </div><!--/form-group--> 
              </form>
                <?php } ?>
                
                <?php if($_GET['action']=="edit"){$id = $_GET['id']; editData($id);} ?>
                
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row--> 
    
      </div>
      <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
  </div>
  <!--\\\\\\\ inner end\\\\\\-->
</div>
<!--\\\\\\\ wrapper end\\\\\\-->     

<script src="js/jquery-2.1.0.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/common-script.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
<script type="text/javascript"  src="plugins/toggle-switch/toggles.min.js"></script> 
<script src="plugins/checkbox/zepto.js"></script>
<script src="plugins/checkbox/icheck.js"></script>
<script src="js/icheck-init.js"></script>
<script type="text/javascript" src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script> 
<script type="text/javascript" src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="js/form-components.js"></script> 
<script type="text/javascript"  src="plugins/input-mask/jquery.inputmask.min.js"></script> 
<script type="text/javascript"  src="plugins/input-mask/demo-mask.js"></script> 
<script type="text/javascript"  src="plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script> 
<script type="text/javascript"  src="plugins/dropzone/dropzone.min.js"></script> 
<script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>



<script src="plugins/validation/parsley.min.js"></script>

<script>


/*==Porlets Actions==*/
    $('.minimize').click(function(e){
      var h = $(this).parents(".header");
      var c = h.next('.porlets-content');
      var p = h.parent();
      
      c.slideToggle();
      
      p.toggleClass('closed');
      
      e.preventDefault();
    });
    
    $('.refresh').click(function(e){
      var h = $(this).parents(".header");
      var p = h.parent();
      var loading = $('&lt;div class="loading"&gt;&lt;i class="fa fa-refresh fa-spin"&gt;&lt;/i&gt;&lt;/div&gt;');
      
      loading.appendTo(p);
      loading.fadeIn();
      setTimeout(function() {
        loading.fadeOut();
      }, 1000);
      
      e.preventDefault();
    });
    
    $('.close-down').click(function(e){
      var h = $(this).parents(".header");
      var p = h.parent();
      
      p.fadeOut(function(){
        $(this).remove();
      });
      e.preventDefault();
    });

</script>

<script src="js/jPushMenu.js"></script> 

<script>

function addAlumni(){
	var fullname= $('#fullname').val();
	var batch = $('#batch').val();
	var branch = $('#branch').val();
	var permanent_address = $('#permanent_address').val();
	var email = $('#email').val();
	var phone = $('#phoneno').val();
	var job = $('#job').val();
	var achievements = $('#achievements').val();
	var regno = $('#regno').val();
    var company =$('#company').val();
    
var alumniString= 'fullname='+fullname+'&batch='+batch+'&branch='+branch+'&permanent_address='+permanent_address+'&email='+email+'&phoneno='+phone+'&job='+job+'&achievements='+achievements+'&regno='+regno+'&add=true&company='+company;
 
    $.ajax({
            type: "POST",
            url: "php/actions.php",
            data: alumniString,
            cache: false,
            beforeSend: function(){ $("#add").html('Inserting <i class="fa fa-spin fa-refresh"></i>');},
            success: function(data){
            if(data)
            {
                if(data=="done"){
		       $('#suc_mess').show();
                $("#add").html('Submit');
    $('#fullname').val('');
	$('#batch').val('');
	$('#branch').val('');
	$('#permanent_address').val('');
	$('#email').val('');
	$('#phoneno').val('+91 ');
	$('#job').val('');
	$('#achievements').val('');
	$('#regno').val('');
    $('#company').val('');
     $('#suc_mess').delay(2000).fadeOut(2000);
            }   
            else{
                $("#add").html('Submit');
            alert(data);
                
            }
            }
            else
            {
             alert('no response from server');
            }
            }
            });
}

</script>
    <script src="js/editAlumni.js"></script>
</body>
</html>
