<?php require_once('engine/lib/template_modules.php') ?>
<?php require_once('engine/lib/functions.php'); session_start(); 
if(empty($_GET['type'])){header("location: search.php?type=normal");}

$base_url="";
?><!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Theme Starz">

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="<?php echo $base_url ?>/assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url ?>/assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url ?>/assets/css/selectize.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url ?>/assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url ?>/assets/css/vanillabox/vanillabox.css" type="text/css">

    <link rel="stylesheet" href="<?php echo $base_url ?>/assets/css/style.css" type="text/css">

<!--    <link rel="stylesheet" href="assets/search/css/reset.css">  CSS reset -->
	<link rel="stylesheet" href="<?php echo $base_url ?>/assets/search/css/style.css">
<!--	<script src="js/modernizr.js"></script>  Modernizr -->
    
    <title><?php echo $_GET['type'] ?> Search - KAAI</title>

</head>

<body class="page-sub-page page-register-sign-in">
<!-- Wrapper -->
<div class="wrapper">
<!-- Header -->
<?php include_once("engine/parts/header.php");print_header("search");?>

<!-- end Header -->

<!-- Breadcrumb -->
<div class="container">
    <ol class="breadcrumb">
        <li><a href="./">Home</a></li>
        <li class="active">Find People</li>
    </ol>
</div>
<!-- end Breadcrumb -->
<br>
<!-- Page Content -->
<div id="page-content">

                            <?php
if(isset($_GET['query']) && !empty($_GET['query'])){
    search_module(true_filter($_GET['type']),true_filter($_GET['query'])); }
    else{
        search_module(true_filter($_GET['type']),"");
    }?> 
</div>
<!-- end Page Content -->

<!-- Footer -->
<?php include_once("engine/parts/footer.php");  ?>

<!-- end Footer -->

</div>
<!-- end Wrapper -->
    
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/selectize.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/jQuery.equalHeights.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/jquery.vanillabox-0.1.5.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/assets/js/custom.js"></script>

    <script src="<?php echo $base_url ?>/assets/search/js/jquery.mixitup.min.js"></script>
<script src="<?php echo $base_url ?>/assets/search/js/main.js"></script>

<script type="text/javascript" src="<?php echo $base_url ?>/engine/ajax/login.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>/engine/ajax/search.js"></script>
</body>
</html>