<?php require_once('engine/lib/template_modules.php') ?>
<?php require_once('engine/lib/functions.php'); session_start(); ?>
<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="KAAI">

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/selectize.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/css/vanillabox/vanillabox.css" type="text/css">
    <link rel="stylesheet" href="assets/css/vanillabox/vanillabox.css" type="text/css">

    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
     <link rel="stylesheet" href="assets/css/fullcalendar.css" type="text/css">
    <link rel="stylesheet" href="assets/css/fullcalendar.print.css" type="text/css">

    <title>KAAI- Karunya Alumni Association of India</title>

</head>

<body class="page-homepage-carousel">
    <div id="fb-root"></div>

<!-- Wrapper -->
<div class="wrapper">
<!-- Header -->
<?php include_once("engine/parts/header.php");  print_header("home");?>
<!-- end Header -->

<div id="page-content">
    <!-- Page Content -->
    <div id="homepage-carousel" style="z-index: 9999">
        <div class="container">
            <div class="homepage-carousel-wrapper">
                <div class="row">
                    <div class="col-md-6 col-sm-7">
                        <div class="image-carousel">
                            <div class="image-carousel-slide"><img src="assets/img/slide-1.jpg" alt=""></div>
                            <div class="image-carousel-slide"><img src="assets/img/slide-2.jpg" alt=""></div>
                            <div class="image-carousel-slide"><img src="assets/img/slide-3.jpg" alt=""></div>
                        </div><!-- /.slider-image -->
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-6 col-sm-5">
                        <div class="slider-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1>Find People On This Awesome Community !</h1>
                                    <form id="slider-form" role="form" action="" method="post">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <input class="form-control has-dark-background" name="slider-name" id="slider-name" placeholder="Full Name [optional]" type="text">
                                                </div>
                                            </div><!-- /.col-md-6 -->
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <input class="form-control has-dark-background" name="slider-email" id="slider-email" placeholder="Reg No [optional]" type="text" >
                                                </div>
                                            </div><!-- /.col-md-6 -->
                                        </div><!-- /.row -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <select name="slider-study-level" id="slider-study-level" class="has-dark-background" required>
                                                        <option value="">Select Batch</option>
                                                        <?php batch_calc("Select"); ?>
                                                    </select>
                                                </div><!-- /.form-group -->
                                            </div><!-- /.col-md-6 -->
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <select name="slider-course" id="slider-course" class="has-dark-background" required>
                                                        <option value="">Courses</option>
                                                        <option value="CSE">CSE</option>
                                                        <option value="CSE2">CSE</option>
                                                        <option value="CSE3">CSE</option>
                                                        <option value="CSE4">CSE</option>

                                                    </select>
                                                </div><!-- /.form-group -->
                                            </div><!-- /.col-md-6 -->
                                        </div><!-- /.row -->
                                        <button type="submit" id="slider-submit" class="btn btn-framed pull-right">Find Them !</button>
                                        <div id="form-status"></div>
                                    </form>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.slider-content -->
                    </div><!-- /.col-md-6 -->
                </div><!-- /.row -->
                <div class="background"></div>
            </div><!-- /.slider-wrapper -->
            <div class="slider-inner"></div>
        </div><!-- /.container -->
    </div>
    <!-- end Slider -->
    <section id="featured-courses">
        <div class="block">
            <div class="container">
                <header>
                  <h2>Upcoming Events</h2>
                </header>
                <div class="row">
                    <div class="events images featured">
                        <div class="col-md-3 col-sm-6">
                            <article class="event">
                                <div class="event-thumbnail">
                                    <figure class="event-image">
                                        <div class="image-wrapper"><img src="assets/img/JC.jpg"></div>
                                    </figure>
                                    <figure class="date">
                                        <div class="month">jan</div>
                                        <div class="day">29</div>
                                    </figure>
                                </div>
                                <aside>
                                    <header>
                                        <a href="event-detail.html">Coimbatore Prayer Festival - VOC Park,Avinashi Road </a>
                                    </header>
                                    <div class="additional-info"><span class="fa fa-map-marker"></span></div>
                                    <a href="event-detail.html" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article><!-- /.event -->
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3 col-sm-6">
                            <article class="event">
                                <div class="event-thumbnail">
                                    <figure class="event-image">
                                        <div class="image-wrapper"><img src="assets/img/CST.jpg"></div>
                                    </figure>
                                    <figure class="date">
                                        <div class="month">FEB</div>
                                        <div class="day">6</div>
                                    </figure>
                                </div>
                                <aside>
                                    <header>
                                        <a href="event-detail.html">CST Alumni Reunion </a>                                    </header>
                                    <div class="additional-info"><span class="fa fa-map-marker"></span></div>
                                    <a href="event-detail.html" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article><!-- /.event -->
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3 col-sm-6">
                            <article class="event">
                                <div class="event-thumbnail">
                                    <figure class="event-image">
                                        <div class="image-wrapper"><img src="assets/img/ECE.jpg"></div>
                                    </figure>
                                    <figure class="date">
                                        <div class="month">feb</div>
                                        <div class="day">6</div>
                                    </figure>
                                </div>
                                <aside>
                                    <header>
                                        <a href="event-detail.html">Electrical Sciences Alumni Homecoming</a>
                                    </header>
                                    <div class="additional-info"><span class="fa fa-map-marker"></span></div>
                                    <a href="event-detail.html" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article><!-- /.event -->
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3 col-sm-6">
                            <article class="event">
                                <div class="event-thumbnail">
                                    <figure class="event-image">
                                        <div class="image-wrapper"><img src="assets/img/mk.jpg"></div>
                                    </figure>
                                    <figure class="date">
                                        <div class="month">mar</div>
                                        <div class="day">3</div>
                                    </figure>
                                </div>
                                <aside>
                                    <header>
                                        <a href="event-detail.html">Mindkraft</a>
                                    </header>
                                    <div class="additional-info"><span class="fa fa-map-marker"></span></div>
                                    <a href="event-detail.html" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article><!-- /.event -->
                        </div><!-- /.col-md-3 -->
                    </div><!-- /.events -->
                </div><!-- /.row -->
            </div><!-- /.container -->
            <div class="background background-color-grey-background"></div>
        </div><!-- /.block -->
    </section>
    <!-- /#featured-courses -->

    <div class="container">
        <div class="row block">
        <!--MAIN Content-->
            <div class="col-md-8">
                <div id="page-main">
                    <aside id="our-professors">
                        <header>
                            <h2>Our Notable Alumnus</h2>
                        </header>
                        <div class="section-content">
                            <div class="professors">
                                <article class="professor-thumbnail">
                                    <figure class="professor-image"><a href="member-detail.html"><img src="assets/img/Picture1.png" alt=""></a></figure>
                                    <aside>
                                        <header>
                                            <a href="#">MR. MATHEW OOMEN<P>PRESIDENT RELIANCE JIO</P></a>
                                            <div class="divider"></div>
                                            <figure class="professor-description">ECE 1986-90</figure>
                                        </header>
                                        <a href="#" class="show-profile">Show Profile</a>
                                    </aside>
                                </article><!-- /.professor-thumbnail -->
                                
                                <article class="professor-thumbnail">
                                    <figure class="professor-image"><a href="member-detail.html"><img src="assets/img/Picture2.png" alt=""></a></figure>
                                    <aside>
                                        <header>
                                            <a href="member-detail.html">MR. SENTHIL MUTHIAH<P>PARTNER MCKINSEY AND COMPANY</P></a>
                                            <div class="divider"></div>
                                            <figure class="professor-description">ECE 1990-94</figure>
                                        </header>
                                        <a href="member-detail.html" class="show-profile">Show Profile</a>
                                    </aside>
                                </article><!-- /.alumnus-thumbnail -->
                                
                                <article class="professor-thumbnail">
                                    <figure class="professor-image"><a href="member-detail.html"><img src="assets/img/Picture3.PNG" alt=""></a></figure>
                                    <aside>
                                        <header>
                                            <a href="member-detail.html">MR. JOSHUA MADAN<P>CEO COVENANT CONSULTANTS</P></a>
                                            <div class="divider"></div>
                                            <figure class="professor-description">ECE 1994-98</figure>
                                        </header>
                                        <a href="member-detail.html" class="show-profile">Show Profile</a>
                                    </aside>
                                </article><!-- /.alumnus-thumbnail -->
                                <article class="professor-thumbnail">
                                    <figure class="professor-image"><a href="member-detail.html"><img src="assets/img/Picture4.png" alt=""></a></figure>
                                    <aside>
                                        <header>
                                            <a href="member-detail.html">MR. SAM RUFUS<P>CEO GLUEPLUS</P></a>
                                            <div class="divider"></div>
                                            <figure class="professor-description">MECH 1989-93</figure>
                                        </header>
                                        <a href="member-detail.html" class="show-profile">Show Profile</a>
                                    </aside>
                                </article><!-- /.alumnus-thumbnail -->
                                
                            </div><!-- /.professors -->
                        </div><!-- /.section-content -->
                    </aside><!-- /.our-professors -->

                    <hr>

                    <section class="event-calendar block">
                        <header><h2>Event Calendar</h2></header>
                        <section id="event-calendar">
                            <div class="calendar"></div>
                        </section>
                    </section><!-- /.event-calendar -->

                    <!-- Testimonial -->
                    <section id="testimonials" class="block">
                        <header><h2>Words from the alumnus</h2></header>
                        <div class="author-carousel">
                            <div class="author">
                                <blockquote>
                                    <figure class="author-picture"><img src="assets/img/student-testimonial.jpg" alt=""></figure>
                                    <article class="paragraph-wrapper">
                                        <div class="inner">
                                            <header>It was awesome to be in karunya !</header>
                                            <footer>Claire Doe</footer>
                                        </div>
                                    </article>
                                </blockquote>
                            </div><!-- /.author -->
                            <div class="author">
                                <blockquote>
                                    <figure class="author-picture"><img src="assets/img/student-testimonial.jpg" alt=""></figure>
                                    <article class="paragraph-wrapper">
                                        <div class="inner">
                                            <header>Morbi nec nisi ante. Quisque lacus ligula, iaculis in elit et, interdum semper quam. Fusce in interdum tortor.
                                                Ut sollicitudin lectus dolor eget imperdiet libero pulvinar sit amet.</header>
                                            <footer>Claire Doe</footer>
                                        </div>
                                    </article>
                                </blockquote>
                            </div><!-- /.author -->
                        </div><!-- /.author-carousel -->
                    </section>
                    <!-- end Testimonial -->
                    
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->
            <!--SIDEBAR Content-->
            <?php side_bar_module();  ?>
            <!-- /.col-md-4 -->
        </div>
    </div>



</div>
<!-- end Page Content -->

<!-- Footer -->
<?php include_once("engine/parts/footer.php");  ?>
<!-- end Footer -->

</div>
<!-- end Wrapper -->
<script type="text/javascript" src="assets/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/selectize.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="assets/js/jQuery.equalHeights.js"></script>
<script type="text/javascript" src="assets/js/icheck.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.vanillabox-0.1.5.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="assets/js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="assets/js/fullcalendar.min.js"></script>

<script type="text/javascript" src="assets/js/custom.js"></script>
    
    <script type="text/javascript" src="engine/ajax/login.js"></script>

    <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>