<?php require_once('engine/lib/template_modules.php'); 
 require_once('engine/lib/functions.php'); session_start(); ?>

<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Theme Starz">

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="/assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="/index.html" type="text/css">
    <link rel="stylesheet" href="/index.html" type="text/css">
    <link rel="stylesheet" href="/assets/css/vanillabox/vanillabox.css" type="text/css">

    <link rel="stylesheet" href="/assets/css/style.css" type="text/css">

    <title>Universo - Educational, Course and University Template</title>

</head>

<body class="page-sub-page page-events-listing">
<!-- Wrapper -->
<div class="wrapper">
<!-- Header -->
<?php include_once("engine/parts/header.php");  print_header("events");?>
<!-- end Header -->

<!-- Breadcrumb -->
<div class="container">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Events</a></li>
    </ol>
</div>
<!-- end Breadcrumb -->

<!-- Page Content -->
<div id="page-content">
    <div class="container">
        <div class="row">
            <!--MAIN Content-->
            <div class="col-md-8">
                <div id="page-main">
                    <section class="events" id="events">
                        <header><h1>Events</h1></header>
                        <div class="section-content">
                            <article class="event">
                                <figure class="date">
                                    <div class="month">jan</div>
                                    <div class="day">18</div>
                                </figure>
                                <aside>
                                    <header>
                                        <a href="event-detail.html">Conservatory Exhibit: The garden of india a country and culture revealed</a>
                                    </header>
                                    <div class="additional-info"><span class="fa fa-map-marker"></span> Matthaei Botanical Gardens</div>
                                    <div class="description">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et urna fringilla
                                            volutpat elit non, tristique lectus. Nam blandit odio nisl, ac malesuada lacus fermentum sit amet.
                                            Vestibulum vitae aliquet felis, ornare feugiat elit. Nulla varius condimentum elit,
                                            sed pulvinar leo sollicitudin vel.
                                        </p>
                                    </div>
                                    <a href="event-detail.html" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article><!-- /article -->
                            <article class="event">
                                <figure class="date">
                                    <div class="month">feb</div>
                                    <div class="day">01</div>
                                </figure>
                                <aside>
                                    <header>
                                        <a href="event-detail.html">February Half-Term Activities: Big Stars and Little Secrets </a>
                                    </header>
                                    <div class="additional-info"><span class="fa fa-map-marker"></span> Pitt Rivers and Natural History Museums</div>
                                    <div class="description">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et urna fringilla
                                            volutpat elit non, tristique lectus. Nam blandit odio nisl, ac malesuada lacus fermentum sit amet.
                                            Vestibulum vitae aliquet felis, ornare feugiat elit. Nulla varius condimentum elit,
                                            sed pulvinar leo sollicitudin vel.
                                        </p>
                                    </div>
                                    <a href="event-detail.html" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article><!-- /article -->
                            <article class="event">
                                <figure class="date">
                                    <div class="month">mar</div>
                                    <div class="day">23</div>
                                </figure>
                                <aside>
                                    <header>
                                        <a href="event-detail.html">The Orchestra of the Age of Enlightenment perform with Music</a>
                                    </header>
                                    <div class="additional-info"><span class="fa fa-map-marker"></span> Faculty of Music</div>
                                    <div class="description">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et urna fringilla
                                            volutpat elit non, tristique lectus. Nam blandit odio nisl, ac malesuada lacus fermentum sit amet.
                                            Vestibulum vitae aliquet felis, ornare feugiat elit. Nulla varius condimentum elit,
                                            sed pulvinar leo sollicitudin vel.
                                        </p>
                                    </div>
                                    <a href="event-detail.html" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article><!-- /article -->
                            <article class="event">
                                <figure class="date">
                                    <div class="month">may</div>
                                    <div class="day">12</div>
                                </figure>
                                <aside>
                                    <header>
                                        <a href="event-detail.html">Museums and the Construction of Identities</a>
                                    </header>
                                    <div class="additional-info"><span class="fa fa-map-marker"></span> Faculty of Music</div>
                                    <div class="description">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et urna fringilla
                                            volutpat elit non, tristique lectus. Nam blandit odio nisl, ac malesuada lacus fermentum sit amet.
                                            Vestibulum vitae aliquet felis, ornare feugiat elit. Nulla varius condimentum elit,
                                            sed pulvinar leo sollicitudin vel.
                                        </p>
                                    </div>
                                    <a href="event-detail.html" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article><!-- /article -->
                            <article class="event">
                                <figure class="date">
                                    <div class="month">jun</div>
                                    <div class="day">17</div>
                                </figure>
                                <aside>
                                    <header>
                                        <a href="event-detail.html">Reporting: In Conversation with Miriam Elder and Julia Ioffe</a>
                                    </header>
                                    <div class="additional-info"><span class="fa fa-map-marker"></span> Faculty of Music</div>
                                    <div class="description">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et urna fringilla
                                            volutpat elit non, tristique lectus. Nam blandit odio nisl, ac malesuada lacus fermentum sit amet.
                                            Vestibulum vitae aliquet felis, ornare feugiat elit. Nulla varius condimentum elit,
                                            sed pulvinar leo sollicitudin vel.
                                        </p>
                                    </div>
                                    <a href="event-detail.html" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article><!-- /article -->
                        </div><!-- /.section-content -->
                    </section><!-- /.events -->
                    <div class="center">
                        <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                        </ul>
                    </div>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->

            <!--SIDEBAR Content-->
                                    <?php side_bar_module();  ?>

       
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
<!-- end Page Content -->

<!-- Footer -->
<?php include_once("engine/parts/footer.php");  ?>
    <!-- end Footer -->

</div>
<!-- end Wrapper -->

<script type="text/javascript" src="/assets/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/selectize.min.js"></script>
<script type="text/javascript" src="/assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="/assets/js/jQuery.equalHeights.js"></script>
<script type="text/javascript" src="/assets/js/icheck.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.vanillabox-0.1.5.min.js"></script>
<script type="text/javascript" src="/assets/js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="/assets/js/custom.js"></script>

</body>
</html>