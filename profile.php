<?php require_once('engine/lib/template_modules.php'); require_once('engine/lib/functions.php');session_start(); check_session(); ?>

<!DOCTYPE html>

<html lang="en-US" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="KAAI">

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="/assets/css/vanillabox/vanillabox.css" type="text/css">

    <link rel="stylesheet" href="assets/css/style.css" type="text/css">

    <title><?php echo $_SESSION['username']; ?> - KAAI</title>

</head>

<body class="page-sub-page page-my-account">
<!-- Wrapper -->
<div class="wrapper">
<!-- Header -->
<?php include_once("engine/parts/header.php");  print_header("dashboard");?>

<!-- end Header -->

<!-- Breadcrumb -->
<div class="container">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active">My Profile</li>
    </ol>
</div>
<!-- end Breadcrumb -->

<!-- Page Content -->
<div id="page-content">
    <div class="container">
        <header><h1>My Account</h1></header>
        <div class="row">
            <div class="col-md-12">
                <section id="my-account">
                    <ul class="nav nav-tabs" id="tabs">
                        <li class="active"><a href="#tab-profile" data-toggle="tab">Profile</a></li>
                        <li><a href="#tab-my-courses" data-toggle="tab">My Jobs</a></li>
                        <li><a href="#tab-my-internships" data-toggle="tab">My Interships</a></li>
                        <li><a href="#tab-change-password" data-toggle="tab">Change Password</a></li>
                    </ul><!-- /#my-profile-tabs -->
                    <div class="tab-content my-account-tab-content">
                        <div class="tab-pane active" id="tab-profile">
                            <section id="my-profile">
                                <header><h3>My Profile</h3></header>
                                <div class="my-profile">
                                    <figure class="profile-avatar">
                                        <div class="image-wrapper"><img src="assets/img/profile-avatar.jpg"></div>
                                    </figure>
                                    <article>
                                        <div class="table-responsive">
                                            <table class="my-profile-table">
                                                
                                                <tbody>
                                                <tr>
                                                    <td class="title">Name<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="name" value="John Doe">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Location/Place<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="location" value="London UK">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Registration No (optional)</td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="regno" value="" placeholder="UR13CS015">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Batch</td>
                                                    <td>
                                                        <div class="input-group">
                                                            <select name="study-level" >
                                                            <?php batch_calc("2000 - 2004"); ?>
                                                                </select>
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Present Company<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="company" value="" placeholder="Bash Labs Inc.">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Designation<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="designation" value="CEO" placeholder="CEO">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Interests<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="interests" value="" placeholder="Networking, Teaching, R&D etc">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title bio">Bio</td>
                                                    <td>
                                                        <div class="input-group">
                                                            <textarea id="bio">Frontend Developer </textarea>
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Website</td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="website" value="www.bashlabs.in" placeholder="www.example.com">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Change Photo</td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="file" id="change-photo">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <button type="submit" class="btn pull-right">Save Changes</button>
                                    </article>
                                </div><!-- /.my-profile -->
                            </section><!-- /#my-profile -->
                        </div><!-- /tab-pane -->
                        <div class="tab-pane" id="tab-my-courses">
                            <section id="course-list">
                                <button class="btn pull-right" data-toggle="modal" data-target="#new-job"><i class="glyphicon glyphicon-plus"></i> New Job</button>
                                <header><h3>My Jobs</h3></header>
                                <table class="table table-hover table-responsive course-list-table tablesorter">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th class="starts">Starts</th>
                                        <th class="status">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="status-not-started">
                                        <th class="course-title"><a href="course-detail-v1.html">Introduction to modo 701</a></th>
                                        <th class="course-category"><a href="#">Graphic Design and 3D</a></th>
                                        <th>01-03-2014</th>
                                        <th class="status"><i class="fa fa-calendar-o"></i>Not started yet</th>
                                    </tr>
                                        <tr class="status-completed">
                                        <th class="course-title"><a href="course-detail-v1.html">Introduction to modo 701</a></th>
                                        <th class="course-category"><a href="#">Graphic Design and 3D</a></th>
                                        <th>01-03-2014</th>
                                        <th class="status"><i class="fa fa-calendar-o"></i>Not started yet</th>
                                    </tr>
                                    
                                    </tbody>
                                </table>
                                <div class="center">
                                    <ul class="pagination">
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                    </ul>
                                </div>
                            </section><!-- /#course-list -->
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="tab-my-internships">
                            <section id="course-list">
                                <button class="btn pull-right" data-toggle="modal" data-target="#new-internship"><i class="glyphicon glyphicon-plus"></i> New Internship</button>
                                <header><h3>My Internships</h3></header>
                                <table class="table table-hover table-responsive course-list-table tablesorter">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th class="starts">Starts</th>
                                        <th class="status">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="status-not-started">
                                        <th class="course-title"><a href="course-detail-v1.html">Introduction to modo 701</a></th>
                                        <th class="course-category"><a href="#">Graphic Design and 3D</a></th>
                                        <th>01-03-2014</th>
                                        <th class="status"><i class="fa fa-calendar-o"></i>Not started yet</th>
                                    </tr>
                                        <tr class="status-completed">
                                        <th class="course-title"><a href="course-detail-v1.html">Introduction to modo 701</a></th>
                                        <th class="course-category"><a href="#">Graphic Design and 3D</a></th>
                                        <th>01-03-2014</th>
                                        <th class="status"><i class="fa fa-calendar-o"></i>Not started yet</th>
                                    </tr>
                                    
                                    </tbody>
                                </table>
                                <div class="center">
                                    <ul class="pagination">
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                    </ul>
                                </div>
                            </section><!-- /#course-list -->
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="tab-change-password">
                            <section id="password">
                                <header><h3>Change Password</h3></header>
                                <div class="row">
                                    <div class="col-md-5 col-md-offset-4">
                                        <p>
                                            Free Advice: Always use a combination of alphanumeric characters and symbols to make your password secure !
                                        </p>
                                        <form role="form" class="clearfix" action="course-joined.html">
                                            <div class="form-group">
                                                <label for="current-password">Current Password</label>
                                                <input type="password" class="form-control" id="current-password">
                                            </div>
                                            <div class="form-group">
                                                <label for="new-password">New Password</label>
                                                <input type="password" class="form-control" id="new-password">
                                            </div>
                                            <div class="form-group">
                                                <label for="repeat-new-password">Repeat New Password</label>
                                                <input type="password" class="form-control" id="repeat-new-password">
                                            </div>
                                            <button type="submit" class="btn pull-right">Change Password</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div><!-- /.tab-content -->
                </section>
            </div>

        <!-- /.col-md-4 -->
            <!-- end SIDEBAR Content-->




        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
<!-- end Page Content -->

    <div id="new-job" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      
        <form>
        <div class="modal-body">
          <div class="my-profile">
          <article>
          <div class="table-responsive">
          <table class="my-profile-table">
                                                <tbody >
                                                <tr>
                                                    <td class="title">Designation <sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="name" placeholder="Suitable title for the job. (Eg: Web Developer" required>
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Type <sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="name" placeholder="IT,Computer Science,Electronics,Photography,Film Making, All etc">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Company Name<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="location" placeholder="Name of the company">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Location/Place<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="location" placeholder="Place,City,Country of the company">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Experience <sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="location" placeholder="0-2 Years">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Salary<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="location" placeholder="in INR">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                <tr>
                                                </tr>
                                                    <td class="title">Job Description<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <textarea  required placeholder="Description in 500 words"></textarea>
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">External Link<sup>(if any)</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="location" placeholder="Paste Link here">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                
                                            
                                                </tbody>
                                            </table>
                                                
      </div>
              </article>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-default">Add</button>
      </div>
    </div>

        </form>
      
  </div>
</div>
    </div>
    <div id="new-internship" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      
        <form>
        <div class="modal-body">
          <div class="my-profile">
          <article>
          <div class="table-responsive">
          <table class="my-profile-table">
                                                <tbody >
                                                <tr>
                                                    <td class="title">Title <sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="name" placeholder="Suitable title for the internship. (Eg: Web Developer" required>
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Category <sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="name" placeholder="IT,Computer Science,Electronics,Photography,Film Making, All etc">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Company Name<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="location" placeholder="Name of the company">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Location/Place<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="location" placeholder="Place,City,Country of the company">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Amount <sup>(if any)</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="location" placeholder="in INR">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">Description<sup>*</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <textarea  required placeholder="Description in 500 words"></textarea>
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="title">External Link to website/pdf/image etc<sup>(if any)</sup></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  required type="text" class="form-control" id="location" placeholder="Paste Link here">
                                                        </div><!-- /input-group -->
                                                    </td>
                                                </tr>
                                                
                                            
                                                </tbody>
                                            </table>
                                                
      </div>
              </article>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-default">Add</button>
      </div>
    </div>

        </form>
      
  </div>
</div>
    </div>
    
    
    
<!-- Footer -->
<?php include_once("engine/parts/footer.php");  ?>
<!-- end Footer -->
</div>
<!-- end Wrapper -->

<script type="text/javascript" src="assets/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/selectize.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="assets/js/jQuery.equalHeights.js"></script>
<script type="text/javascript" src="assets/js/icheck.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.vanillabox-0.1.5.min.js"></script>
<script type="text/javascript" src="assets/js/countdown.js"></script>
<script type="text/javascript" src="assets/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="assets/js/retina-1.1.0.min.js"></script>

<script type="text/javascript" src="assets/js/custom.js"></script>

    <script type="text/javascript" src="engine/ajax/login.js"></script>

</body>
</html>