<?php require_once('engine/lib/template_modules.php') ?>

<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Theme Starz">

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/selectize.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/css/vanillabox/vanillabox.css" type="text/css">

    <link rel="stylesheet" href="assets/css/style.css" type="text/css">

    <title>Universo - Educational, Course and University Template</title>

</head>

<body class="page-sub-page page-member-detail">
<!-- Wrapper -->
<div class="wrapper">
<!-- Header -->
<?php include_once("engine/parts/header.php");  print_header("alumni");?>
<!-- end Header -->

<!-- Breadcrumb -->
<div class="container">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Members</a></li>
        <li class="active">Member Detail</li>
    </ol>
</div>
<!-- end Breadcrumb -->

<!-- Page Content -->
<div id="page-content">
    <div class="container">
        <div class="row">
            <!--MAIN Content-->
            <div class="col-md-8">
                <div id="page-main">
                    <section id="members">
                        <header><h1>Member Detail</h1></header>
                        <div class="author-block member-detail">
                            <figure class="author-picture"><img src="assets/img/member-detail.jpg" alt=""></figure>
                            <article class="paragraph-wrapper">
                                <div class="inner">
                                    <header><h2>Claire Doe</h2></header>
                                    <figure>Marketing Specialist</figure>
                                    <hr>
                                    <p class="quote">
                                        Morbi nec nisi ante. Quisque lacus ligula, iaculis in elit et, interdum semper quam. Fusce in interdum tortor.
                                    </p>
                                    <hr>
                                    <div class="contact">
                                        <strong>Contact Member</strong>
                                        <div class="icons">
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-pinterest"></i></a>
                                            <a href=""><i class="fa fa-youtube-play"></i></a>
                                        </div><!-- /.icons -->
                                    </div>
                                    <h3>Biography</h3>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut mauris orci, interdum in purus at,
                                        mollis dignissim augue. Praesent sit amet lacinia mauris, vitae placerat orci. Suspendisse pretium
                                        tincidunt volutpat. Nunc laoreet nulla et dictum mollis. Sed convallis elit justo, in vulputate
                                        augue condimentum a. Fusce sit amet malesuada erat. Fusce facilisis tristique nunc, non scelerisque
                                        lacus bibendum at. Sed tellus ligula, scelerisque eu arcu ac, adipiscing tincidunt risus. Fusce sed
                                        quam quis massa accumsan consectetur in vitae justo.
                                    </p>
                                    <p>
                                        Cras at elit vel ante cursus dapibus eu sit amet
                                        nibh. Maecenas egestas aliquam risus pulvinar tincidunt. Quisque nibh magna, malesuada id eros ut,
                                        commodo hendrerit sem. Maecenas iaculis mi eget eros sollicitudin pharetra. Integer mi nibh, pellentesque
                                        quis nibh sit amet, faucibus aliquet purus. Duis ultrices magna venenatis nibh sagittis, in suscipit nisl
                                        semper. Fusce egestas metus id vehicula aliquet.
                                    </p>
                                    <h3>Member's Courses</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover course-list-table tablesorter">
                                            <thead>
                                            <tr>
                                                <th>Course Name</th>
                                                <th class="starts">Starts</th>
                                                <th class="length">Length</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th class="course-title"><a href="course-detail-v1.html">Introduction to modo 701</a></th>
                                                <th>01-03-2014</th>
                                                <th>3 months</th>
                                            </tr>
                                            <tr>
                                                <th class="course-title"><a href="course-detail-v1.html">Become self marketer</a></th>
                                                <th>03-03-2014</th>
                                                <th>2 lessons</th>
                                            </tr>
                                            <tr>
                                                <th class="course-title"><a href="course-detail-v1.html">How to find long term customers</a></th>
                                                <th>06-03-2014</th>
                                                <th>1 month</th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </article>
                        </div><!-- /.author -->
                    </section>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->

            <!--SIDEBAR Content-->
                        <?php side_bar_module();  ?>
<!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
<!-- end Page Content -->

<!-- Footer -->
<?php include_once("engine/parts/footer.php");  ?>
<!-- end Footer -->

</div>
<!-- end Wrapper -->

<script type="text/javascript" src="assets/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/selectize.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="assets/js/jQuery.equalHeights.js"></script>
<script type="text/javascript" src="assets/js/icheck.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.vanillabox-0.1.5.min.js"></script>
<script type="text/javascript" src="assets/js/retina-1.1.0.min.js"></script>

<script type="text/javascript" src="assets/js/custom.js"></script>

</body>
</html>