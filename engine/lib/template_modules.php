<?php
include_once("functions.php");
function side_bar_module(){
echo '<div class="col-md-4">
                <div id="page-sidebar" class="sidebar">
                    <aside class="news-small" id="news-small">
                        <header>
                            <h2>Latest Jobs</h2>
                        </header>
                        <div class="section-content">
                            <article>
                                <figure class="date"><i class="fa fa-file-o"></i>08-24-2014</figure>
                                <header><a href="#">Web Designer at Bash Labs</a></header>
                            </article><!-- /article -->
                            <article>
                                <figure class="date"><i class="fa fa-file-o"></i>08-24-2014</figure>
                                <header><a href="#">Web Designer at Bash Labs</a></header>
                            </article><!-- /article -->
                            <article>
                                <figure class="date"><i class="fa fa-file-o"></i>08-24-2014</figure>
                                <header><a href="#">Web Designer at Bash Labs</a></header>
                            </article><!-- /article -->
                            
                        </div><!-- /.section-content -->
                        <a href="" class="read-more">Visit Job Portal</a>
                    </aside><!-- /.news-small -->
                    <aside id="newsletter">
                        <header>
                            <h2>Newsletter</h2>
                        </header>
                            <div class="section-content">
                                <div class="newsletter">
                                    <div class="input-group">
                                            <span class="input-group-btn">
                                                <button type="submit" class="btn">View our Newletter <i class="fa fa-angle-right"></i></button>
                                            </span>
                                    </div><!-- /input-group -->
                                </div><!-- /.newsletter -->
                               
                            </div><!-- /.section-content -->
                    </aside><!-- /.newsletter -->
                    <aside id="video">
                        <header>
                            <h2>Video</h2>
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/T4nLjWqfiZ4" frameborder="0" allowfullscreen></iframe>
                        </header>
                    </aside>
                    <aside id="newsletter">
                    <div class="fb-page" data-href="https://www.facebook.com/BashLabs" data-width="360" data-height="300" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/BashLabs"><a href="https://www.facebook.com/BashLabs">BashLabs</a></blockquote></div></div>
                    </aside>
                    <aside id="video">
                    <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/amithkraji/favorites" data-widget-id="625791793318096896">Tweets by @amithkraji</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                    </aside>
                </div><!-- /#sidebar -->
            </div>';
}

function search_result($qu){
    $base_url = "";
    $q = strip_tags(stripcslashes($qu));
    if(db_access() == true){
    $result = mysql_query("select * from alumni_profile where fname like '$q%' or lname like '$q%'") or die(mysql_error());
        if(mysql_num_rows($result)!=0){
         while($dat = mysql_fetch_array($result)){
?>
<li class="mix color-1 check1 radio2 option3">
                <?php
                                                  $image = mysql_query("select * from user_images where u_id = ".$dat['u_id']);
                                                  $pic = mysql_fetch_array($image);
                                           echo '<img src="data:image/jpeg;base64,'.base64_encode( $pic['image'] ).'"/>' ?>
                <div class="info" style="max-width: 54%">
                <span><?php echo $dat['fname']." ".$dat['lname'] ?></span><br><br>
                <span><?php echo $dat['duration_to']." - ".$dat['duration_from']?></span><br>
                <span><?php echo $dat['current_pos']?></span><br>
                <span><?php echo $dat['city']?></span><br>
                    <span><a href="/alumni/u/<?php echo $dat['u_id']?>" class="read-more">View Profile</a></span>
                </div>
                </li>
<?php
}
        }
        else 
            echo "No Results found for:".$qu;
    }
}

function search_module($type,$query){
    $base_url = "";
    $filter_btn="";
    $normal = "";
    $map = "";
    $resources = "";
    if($type=="normal"){ $normal = "selected"; $filter_btn='<a href="#0" class="cd-filter-trigger">Filters</a>
';}
    elseif($type=="resources"){ $resources = "selected";}
    elseif($type=="map"){ $map = "selected";}
    else{
        echo "<p align='center'> Invalid Type. Please Choose a Search type.</p><br>
       <p align='center'> <a href='../normal' class='read-more'>Normal Search</a><br>
        <a href='../resources' class='read-more'>Search by resources</a><br>
        <a href='../map' class='read-more'>Search by map</a>
        </p>";
 die();
    }
?>
<main class="cd-main-content">
		<div class="cd-tab-filter-wrapper">
			<div class="cd-tab-filter">
				<ul class="cd-filters">
					<li class="placeholder"> 
						<a data-type="all" href="#0">All</a> <!-- selected option on mobile -->
					</li> 
					<li class="filter"><a class="<?php echo "$normal"; ?>" href="<?php echo $base_url; ?>/search/normal">Normal</a></li>
					<li class="filter"><a class="<?php echo "$resources"; ?>" href="<?php echo $base_url; ?>/search/resources" >By Resources</a></li>
					<li class="filter"><a class="<?php echo "$map"; ?>" href="<?php echo $base_url; ?>/search/map">By Map</a></li>
				</ul> <!-- cd-filters -->
			</div> <!-- cd-tab-filter -->
		</div> <!-- cd-tab-filter-wrapper -->

		<section class="cd-gallery">
			<ul>
				
                <?php 
if(!empty($query) && $type == "normal"){
                      search_result($query);

}
    elseif($type == "normal"){
                ?>
                <div class="col-md-12" style="min-height: 50px;position: relative;left:auto;right:auto;">
                    <form class="form-control" style="overflow: hidden;height:auto;display:block;padding: 20px;" action="search.php?type=normal&" method="get" id="searchForm" onsubmit="runSearch('normal');return false;">
                                    <div class="input-group">
<!--                                        <input type="hidden" value="normal" name="type">-->
                                        <input type="text" class="form-control" placeholder="Enter query" name="query" id="query">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn">Find <i class="fa fa-angle-right"></i></button>
                                    </span>
                                    </div>
                        </form>                
                                </div>
                <?php
                }
                ?>
                
				<li class="gap"></li>
				<li class="gap"></li>
				<li class="gap"></li>
			</ul>
<!--			<div class="cd-fail-message">No results found</div>-->
		</section> <!-- cd-gallery -->

		<div class="cd-filter">
			<form>
				<div class="cd-filter-block">
					<h4>Search</h4>
					
					<div class="cd-filter-content">
						<input type="search" placeholder="Try color-1...">
					</div> <!-- cd-filter-content -->
				</div> <!-- cd-filter-block -->

				<div class="cd-filter-block">
					<h4>Check boxes</h4>

					<ul class="cd-filter-content cd-filters list">
						<li>
							<input class="filter" data-filter=".check1" type="checkbox" id="checkbox1">
			    			<label class="checkbox-label" for="checkbox1">Option 1</label>
						</li>

						<li>
							<input class="filter" data-filter=".check2" type="checkbox" id="checkbox2">
							<label class="checkbox-label" for="checkbox2">Option 2</label>
						</li>

						<li>
							<input class="filter" data-filter=".check3" type="checkbox" id="checkbox3">
							<label class="checkbox-label" for="checkbox3">Option 3</label>
						</li>
					</ul> <!-- cd-filter-content -->
				</div> <!-- cd-filter-block -->

				<div class="cd-filter-block">
					<h4>Select</h4>
					
					<div class="cd-filter-content">
						<div class="cd-select cd-filters">
							<select class="filter" name="selectThis" id="selectThis">
								<option value="">Choose an option</option>
								<option value=".option1">Option 1</option>
								<option value=".option2">Option 2</option>
								<option value=".option3">Option 3</option>
								<option value=".option4">Option 4</option>
							</select>
						</div> <!-- cd-select -->
					</div> <!-- cd-filter-content -->
				</div> <!-- cd-filter-block -->

				<div class="cd-filter-block">
					<h4>Radio buttons</h4>

					<ul class="cd-filter-content cd-filters list">
						<li>
							<input class="filter" data-filter="" type="radio" name="radioButton" id="radio1" checked>
							<label class="radio-label" for="radio1">All</label>
						</li>

						<li>
							<input class="filter" data-filter=".radio2" type="radio" name="radioButton" id="radio2">
							<label class="radio-label" for="radio2">Choice 2</label>
						</li>

						<li>
							<input class="filter" data-filter=".radio3" type="radio" name="radioButton" id="radio3">
							<label class="radio-label" for="radio3">Choice 3</label>
						</li>
					</ul> <!-- cd-filter-content -->
				</div> <!-- cd-filter-block -->
			</form>

			<a href="#0" class="cd-close"><i class="fa fa-fw fa-close"></i></a>
		</div> <!-- cd-filter -->

<?php echo $filter_btn; ?>	
    </main> <!-- cd-main-content -->
<?php 

}
?>
