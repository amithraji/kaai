<?php
//header("Access-Control-Allow-Origin: *");


require_once('db.php');
require_once('Slim/Slim.php');

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

$app->add(new \Slim\Middleware\ContentTypes());

$app->contentType('application/json');


/* /user APIs */

/* get user details*/
$app->get('/user/:id',function($id){
    $con = getDB();
    $user_data=mysqli_query($con,"SELECT * FROM `alumni_profile` WHERE `u_id`=$id") or die(throw_error(mysqli_error($con)));

    $data = array();
while($d = mysqli_fetch_assoc($user_data)) {
    $data = $d;
}  
    print json_encode(array("status"=>"success","Message"=>"Here is the detail of the user","data"=>$data));
    mysqli_close($con);
    exit();
    
}); 

$app->put('/user/:id',function($id) use ($app) {
    $con = getDB();
    
    $req = $app->request->getBody();
    $req = json_encode($req);
    $req = json_decode($req);

    if (is_null($req)) {
            http_response_code(400);
            print json_encode(array("status"=>"fail","message" => "Couldn't decode the request", "invalid_json_input" => $req));
        mysqli_close($con);
        }

    $fname = $req->data->fname;
    $lname = $req->data->lname;
    $dept = $req->data->dept;
    $course = $req->data->course;
    $duration_from = $req->data->duration_from;
    $duration_to = $req->data->duration_to;
    $dob = $req->data->dob;
    $marital_status = $req->data->marital_status;
    $dom = $req->data->dom;
    $gender = $req->data->gender;
    $address = $req->data->address;
    $country = $req->data->country;
    $city = $req->data->city;
    $phone = $req->data->phone;
    $email = $req->data->email;
    $about_me = $req->data->about_me;
    $current_pos = $req->data->current_pos;
    $memories = $req->data->memories;
    $expectations = $req->data->expectations;
    $fb = $req->data->fb;
    $tt = $req->data->tt;
    
    $updated = mysqli_query($con,"UPDATE  `kaai`.`alumni_profile` SET  `fname` =  '$fname',
`lname` =  '$lname',
`dept` =  '$dept',
`gender` =  '$gender',
`course` =  '$course',
`duration_from` =  '$duration_from',
`duration_to` =  '$duration_to',
`dob` =  '$dob',
`marital_status` =  '$marital_status',
`dom` = '$dom',
`address` =  '$address',
`country` =  '$country',
`city` =  '$city',
`phone` =  '$phone',
`email` =  '$email',
`about_me` =  '$about_me',
`current_pos` =  '$current_pos',
`memories` =  '$memories',
 `expectations` = '$expectations',
`fb` =  '$fb',
`tt` =  '$tt' WHERE  `alumni_profile`.`u_id`=$id;") or die(throw_error(mysqli_error($con)));
    
    if($updated){
   print json_encode(array("status"=>"success","message"=>"User data successfully updated"));
    }
    mysqli_close($con);
    exit();
}); 
$app->run();
?>