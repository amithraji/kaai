function log_me_in(){
event.preventDefault();
    var username = $('#email').val();
	var password= $('#password').val();
    
var logDispatch= 'username='+username+'&password='+password+'&carrier=ajax&purpose=login';
 
    $.ajax({
            type: "POST",
            url: "engine/cpu/auth_module.php",
            data: logDispatch,
            cache: false,
            beforeSend: function(){ $("#login-btn").html('<i class="fa fa-spin fa-spinner"></i> Verifying');},
            success: function(data){
            if(data)
            {
                if(data=="loginsuccess"){
                    console.log(data);
                $("#login-btn").html('Redirecting . .');
                window.location.assign("profile");
            }   
            else{
                $("#login-btn").html('Try Again');
                console.log(data);
                return false;
                
            }
            }
            else
            {
             alert('no response from server');
            }
            }
            });
}

function log_me_out(){
var logoutDispatch= 'carrier=ajax&purpose=logout';
 
    $.ajax({
            type: "POST",
            url: "engine/cpu/auth_module.php",
            data: logoutDispatch,
            cache: false,
            beforeSend: function(){ $("#logout").html('<i class="fa fa-spin fa-spinner"></i> Logging Out'); console.log(logoutDispatch);
 },
            success: function(lout){
            if(lout)
            {
                if(lout=="logoutsuccess"){
                    console.log(lout);
                location.assign("login");
                }
            else{
                alert("Error Occured ! couldnt logout ! Try clearing history !"); 
                                    console.log(lout);

                return false;
                
            }
            }
            else
            {
             alert('No response from server ! Try reloading the page !');
            }
                
            }
            });
    return false;
}
