<?php $base_url=""; ?>
<footer id="page-footer" style="bottom: 0px;">
    <section id="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <aside class="logo">
                        <img src="<?php echo $base_url ?>/assets/img/logo.png" class="vertical-center" width="80%">
                    </aside>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <header><h4>Contact Us</h4></header>
                        <address>
                            <strong>Alumni Cell, Karunya University</strong>
                            <br>
                            <span>Karunya Nagar</span>
                            <br>
                            <span>Coimbatore, Tamil Nadu</span>
                            <br><br>
                            <br>
                            <abbr title="Telephone">Telephone:</abbr> +91 8015638559
                            <br>
                            <abbr title="Email">Email:</abbr> <a href="#">alumni@karunya.edu</a>
                        </address>
                    </aside>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <header><h4>Important Links</h4></header>
                        <ul class="list-links">
                            <li><a href="#">Future Students</a></li>
                            <li><a href="#">Alumni</a></li>
                            <li><a href="#">Give a Donation</a></li>
                            <li><a href="#">Professors</a></li>
                            <li><a href="#">Libary & Health</a></li>
                            <li><a href="#">Research</a></li>
                        </ul>
                    </aside>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <header><h4>About KAA</h4></header>
                        <p>This paragraph is all about KAA.
                        </p>
                        <div>
                        </div>
                    </aside>
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="background"><img src="<?php echo $base_url; ?>/assets/img/100.png" class="" alt=""></div>
    </section><!-- /#footer-content -->

    <section id="footer-bottom" >
        <div class="container">
            <div class="footer-inner">
                <div class="copyright">© KAA, All rights reserved</div><!-- /.copyright -->
                <dev style="float:right"><a href="#" style="color: #ffffff !important;">About Developers</a></dev>
            </div><!-- /.footer-inner -->
        </div><!-- /.container -->
    </section><!-- /#footer-bottom -->

</footer>