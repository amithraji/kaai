<?php
$_SERVER['DOCUMENT_ROOT']="/kaai";
function print_header($page){
$base_url=$_SERVER['DOCUMENT_ROOT'];
    
    if(isset($_SESSION['username'])){
        $username = $_SESSION['username'];
    $links = '
    <li><a href="'.$base_url.'/profile"><i class="fa fa-user"></i>'.$username.'</a></li>
                <li><a href="#" id="logout" onclick="log_me_out();"><i class="fa fa-sign-out"></i>Logout</a></li>
                ';
    }
    
    else {
    $links = '<li><a href="'.$base_url.'/login"><i class="fa fa-sign-in"></i>Register or Sign In</a></li>';
    }
    
    $home="";
    $career="";
    $donation="";
    $gallery="";
    $events="";
    $search="";
    $dashboard="";
    
    if($page=="home"){
$home ="active";
    }
   elseif($page=="search"){
$search ="active";
    }
    elseif($page=="career"){
$career ="active";
    }
    
    elseif($page=="events"){
$events ="active";
    }
    elseif($page=="gallery"){
$gallery ="active";
    }
    elseif($page=="donation"){  
$donation ="active";
    }
echo '<div class="navigation-wrapper">
    <div class="secondary-navigation-wrapper">
        <div class="container">
            <div class="navigation-contact pull-left"><span class="opacity-70"><a href="/contact-us">Contact Us</a></span></div>
            <div class="search">
            </div>
            <ul class="secondary-navigation list-unstyled">
                '.$links.'
                <li></li>
            </ul>
        </div>
    </div><!-- /.secondary-navigation -->
    <div class="primary-navigation-wrapper">
        <header class="navbar" id="top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand nav" id="brand" style="">
                        <a href="/"><img src="'.$base_url.'/assets/img/logo.png" alt="brand"></a>
                    </div>
                </div>
                <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                    <ul class="nav navbar-nav" style=" font-weight: bolder;
">
                        <li class="'.$home.'">
                            <a href="'.$base_url.'/" class="no-link"><i class="fa fa-home"></i> Home</a>
                           
                        </li>
                        <li  class="'.$search.'">
                            <a href="#" class=" has-child no-link"><i class="fa fa-users"></i> Find People</a>
                            <ul class="list-unstyled child-navigation" style="z-index:9999">
                                <li><a href="'.$base_url.'/search/normal">Normal search</a></li>
                                <li><a href="'.$base_url.'/search/resources">Search by resources</a></li>
                                <li><a href="'.$base_url.'/search/map">Search on map</a></li>
                            </ul>
                        </li>
                        <li  class="'.$events.'">
                            <a href="'.$base_url.'/events"><i class="fa fa-calendar"></i> Events</a></li>
                        <li  class="'.$career.'">
                            <a href="#" class="has-child no-link"><i class="fa fa-paper-plane"></i> Career</a>
                            <ul class="list-unstyled child-navigation">
                                <li><a href="'.$base_url.'/internships">Internships</a></li>
                                <li><a href="'.$base_url.'/jobs">Job Portal</a></li>
                            </ul>
                        </li>
                        <li  class="'.$gallery.'">
                            <a href="'.$base_url.'/gallery"><i class="fa fa-photo"></i> Gallery</a>
                        </li>
                        <li class="'.$donation.'">
                            <a href="#"><i class="fa fa-dollar"></i> Make Donation</a>
                        </li>
                    </ul>
                </nav><!-- /.navbar collapse-->
            </div><!-- /.container -->
        </header><!-- /.navbar -->
    </div><!-- /.primary-navigation -->
    <div class="background">
        <img src="'.$base_url.'/assets/img/100.png"  alt="background">
    </div>
</div>';
}
?>